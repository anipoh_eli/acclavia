package com.elieli.signup;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class form1Activity extends AppCompatActivity {

    public static final String KEY_FULLNAME_MWANACHAMA = "mwanachama_jinaKamili";
    public static final String KEY_DOB_MWANACHAMA = "mwanachama_tareheKuzaliwa";
    public static final String KEY_NDOA_MWANACHAMA = "mwanachama_haliNdoa";
    public static final String KEY_JINSIA_MWANACHAMA = "mwanachama_jinsia";
    public static final String KEY_ANUANI_MWANACHAMA = "mwanachama_anuani";
    private static String STRING_EMPTY = "";
    //private static final String KEY_UHUSIANO_MWANACHAMA = "mwanachama_uhusiano";
    //private static final String KEY_SIMU_MWANACHAMA = "mwanachama_simuNamba";
    private Toolbar toolbar;
    private  SessionManager session;

    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };


    public EditText mwanachamaJinaKamiliEditText;
    public EditText mwanachamaTareheKuzaliwaEditText;
    public EditText mwanachamaNdoaEditText;
   // public Spinner mwanachamaNdoaSpinner;
    public EditText mwanachamaJinsiaEditText;
    public EditText mwanachamaAnuaniEditText;
    public Spinner mwanachamaNdoaSpinner;
    public Spinner mwanachamaGenderSpinner;
    //private EditText mwanachamaSimuNambaEditText;
    //private EditText viewsEditText;
    public String mwanachamaJinaKamili;
    public String mwanachamaTareheKuzaliwa;
    public String mwanachamaNdoa;
    public String marrigeStatus;
    public String mwanachamaJinsia;
    public String mwanachamaGender;
    public String mwanachamaAnuani;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        setToolBar();
        session = new SessionManager(getApplicationContext());
        mwanachamaJinaKamiliEditText = (EditText) findViewById(R.id.mwanachamaJinaKamiliEditText);
        mwanachamaTareheKuzaliwaEditText = (EditText) findViewById(R.id.mwanachamaTareheKuzaliwaEditText);
        mwanachamaNdoaEditText = (EditText) findViewById(R.id.mwanachamaNdoaEditText);
        //TODO sppiner
        mwanachamaNdoaSpinner = (Spinner) findViewById(R.id.spinner_marriage);
        mwanachamaGenderSpinner = (Spinner) findViewById(R.id.spinner_gender);

        mwanachamaJinsiaEditText = (EditText) findViewById(R.id.mwanachamaJinsiaEditText);
        mwanachamaAnuaniEditText = (EditText) findViewById(R.id.mwanachamaAnuaniEditText);

        //ArrayAdapter marriageStatus = new ArrayAdapter(this,R.layout.sim);



        //mtegemeziSimuNamba =

//        Intent intent = new Intent(form1Activity.this,form3Activity.class);
//        intent.putExtra(KEY_FULLNAME_MWANACHAMA,mwanachamaJinaKamili);
//        intent.putExtra(KEY_DOB_MWANACHAMA,mwanachamaTareheKuzaliwa);
//        intent.putExtra(KEY_JINSIA_MWANACHAMA,mwanachamaJinsia);
//        intent.putExtra(KEY_ANUANI_MWANACHAMA,mwanachamaAnuani);

        //Hali ya ndoa spinner
        ArrayAdapter<CharSequence> marriageAdpter = ArrayAdapter.createFromResource
                (this,R.array.array_marriage,R.layout.support_simple_spinner_dropdown_item);
        mwanachamaNdoaSpinner.setAdapter(marriageAdpter);
        mwanachamaNdoaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mwanachamaNdoa = mwanachamaNdoaSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Gender spinner
        ArrayAdapter<CharSequence> genderAdpter = ArrayAdapter.createFromResource
                (this,R.array.array_gender,R.layout.support_simple_spinner_dropdown_item);
        mwanachamaGenderSpinner.setAdapter(genderAdpter);
        mwanachamaGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mwanachamaJinsia = mwanachamaGenderSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mwanachamaTareheKuzaliwaEditText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(form1Activity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



        Button buttonNext = (Button) findViewById(R.id.btn_next1);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!STRING_EMPTY.equals(mwanachamaJinaKamiliEditText.getText().toString()) &&
                        !STRING_EMPTY.equals(mwanachamaTareheKuzaliwaEditText.getText().toString()) &&
                        STRING_EMPTY.equals(mwanachamaJinsiaEditText.getText().toString()) &&
                        !STRING_EMPTY.equals(mwanachamaAnuaniEditText.getText().toString())){

                    mwanachamaJinaKamili = mwanachamaJinaKamiliEditText.getText().toString();
                    mwanachamaTareheKuzaliwa = mwanachamaTareheKuzaliwaEditText.getText().toString();
                    //mwanachamaNdoa = mwanachamaNdoaEditText.getText().toString();
                    mwanachamaNdoa = mwanachamaNdoaSpinner.getSelectedItem().toString();
                    //mwanachamaJinsia = mwanachamaJinsiaEditText.getText().toString();
                    mwanachamaJinsia = mwanachamaGenderSpinner.getSelectedItem().toString();
                    mwanachamaAnuani = mwanachamaAnuaniEditText.getText().toString();

                    Intent intent = new Intent(form1Activity.this,form2Activity.class);
                    intent.putExtra(KEY_FULLNAME_MWANACHAMA,mwanachamaJinaKamili);
                    intent.putExtra(KEY_DOB_MWANACHAMA,mwanachamaTareheKuzaliwa);
                    intent.putExtra(KEY_NDOA_MWANACHAMA,mwanachamaNdoa);
                    intent.putExtra(KEY_JINSIA_MWANACHAMA,mwanachamaJinsia);
                    intent.putExtra(KEY_ANUANI_MWANACHAMA,mwanachamaAnuani);

                    startActivity(intent);


                    //views = viewsEditText.getText().toString();
                    //new form3Activity.ongezaMwanachamaAsyncTask().execute();
                } else {
                    Toast.makeText(form1Activity.this,
                            "One or more fields left empty!",
                            Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(),mwanachamaNdoa,Toast.LENGTH_LONG).show();

                }

            }
        });


    }

    private void setToolBar(){
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    private void updateLabel() {
        String myFormat = "MM-dd-yyy";
        //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        mwanachamaTareheKuzaliwaEditText.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.logout:
                session.setLogin(false);
                Intent intent = new Intent(getBaseContext(), loginActivity.class);
                startActivity(intent);
                finish();
                return true;
            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
