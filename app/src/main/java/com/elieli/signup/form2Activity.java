package com.elieli.signup;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static com.elieli.signup.form1Activity.KEY_ANUANI_MWANACHAMA;
import static com.elieli.signup.form1Activity.KEY_DOB_MWANACHAMA;
import static com.elieli.signup.form1Activity.KEY_FULLNAME_MWANACHAMA;
import static com.elieli.signup.form1Activity.KEY_JINSIA_MWANACHAMA;
import static com.elieli.signup.form1Activity.KEY_NDOA_MWANACHAMA;

public class form2Activity extends AppCompatActivity {

    public static final String KEY_WILAYA_MWANACHAMA = "mwanachama_wilaya";
    public static final String KEY_MKOA_MWANACHAMA = "mwanachama_mkoa";
    public static final String KEY_SIMU_MWANACHAMA = "mwanachama_simu";
    public static final String KEY_BARUA_MWANACHAMA = "mwanachama_barua";
    public static final String KEY_USAJILI_MWANACHAMA = "mwanachama_usajili";
    private Toolbar toolbar;
    private SessionManager session;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };
    private static String STRING_EMPTY = "";
    public EditText mwanachamaWilayaEditText;
    public EditText mwanachamaMkoaEditText;
    public EditText mwanachamaSimuEditText;
    // public Spinner mwanachamaNdoaSpinner;
    public EditText mwanachamaBaruaPepeEditText;
    public EditText mwanachamaTareheUsajiliEditText;


    public String mwanachamaJinaKamili;
    //public String mwanachamaJinaKamili;
    public String mwanachamaTareheKuzaliwa;
    public String mwanachamaNdoa;
    public String mwanachamaJinsia;
    public String mwanachamaAnuani;
    public String mwanachamaWilaya;
    public String mwanachamaMkoa;
    public String mwanachamaSimu;
    public String mwanachamaBaruaPepe;
    public String mwanachamaTareheUsajili;
    private String validphone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form2);
setToolBar();
        session = new SessionManager(getApplicationContext());
        mwanachamaWilayaEditText = (EditText) findViewById(R.id.mwanachamaWilayaEditText);
        mwanachamaMkoaEditText = (EditText) findViewById(R.id.mwanachamaMkoaEditText);
        mwanachamaSimuEditText = (EditText) findViewById(R.id.mwanachamaSimuEditText);
        //mwanachamaNdoaSpinner = (Spinner) findViewById(R.id.spinner_marriage);
        mwanachamaBaruaPepeEditText = (EditText) findViewById(R.id.mwanachamaBaruaPepeEditText);
        mwanachamaTareheUsajiliEditText = (EditText) findViewById(R.id.mwanachamaTareheUsajiliEditText);


        //getting form 1 contents
        final Intent intent = getIntent();
        mwanachamaJinaKamili = intent.getStringExtra(KEY_FULLNAME_MWANACHAMA);
        mwanachamaTareheKuzaliwa = intent.getStringExtra(KEY_DOB_MWANACHAMA);
        mwanachamaNdoa = intent.getStringExtra(KEY_NDOA_MWANACHAMA);
        mwanachamaJinsia = intent.getStringExtra(KEY_JINSIA_MWANACHAMA);
        mwanachamaAnuani = intent.getStringExtra(KEY_ANUANI_MWANACHAMA);

        mwanachamaTareheUsajiliEditText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(form2Activity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        //Toast.makeText(getApplicationContext(),mwanachamaJinaKamili,Toast.LENGTH_LONG).show();

        Button buttonNext = (Button) findViewById(R.id.btn_next2);
        Button buttonBack = (Button) findViewById(R.id.btn_back2);



        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!STRING_EMPTY.equals(mwanachamaWilayaEditText.getText().toString()) &&
                        !STRING_EMPTY.equals(mwanachamaMkoaEditText.getText().toString()) &&
                        !STRING_EMPTY.equals(mwanachamaSimuEditText.getText().toString()) &&
                        !STRING_EMPTY.equals(mwanachamaBaruaPepeEditText.getText().toString())&&
                        !STRING_EMPTY.equals(mwanachamaTareheUsajiliEditText.getText().toString())){

                    //form2 fields
                    mwanachamaWilaya = mwanachamaWilayaEditText.getText().toString();
                    mwanachamaMkoa = mwanachamaMkoaEditText.getText().toString();
                    validphone = mwanachamaSimuEditText.getText().toString();
                    mwanachamaBaruaPepe = mwanachamaBaruaPepeEditText.getText().toString();
                    mwanachamaTareheUsajili = mwanachamaTareheUsajiliEditText.getText().toString();

                    if(validphone.length() < 10 || validphone.length() > 14) {
                        Snackbar.make(v, "Please enter valid phonenumber!", Snackbar.LENGTH_LONG)
                                .show();
                        return;
                    }

            if(!utils.isValidEmail(mwanachamaBaruaPepe)){
                        Snackbar.make(v, "Please Enter a valid email " , Snackbar
                                .LENGTH_LONG)
                                .show();
                        return;
                    }
                    else {
                        mwanachamaSimu = GetCountryZipCode() + validphone.substring(validphone.length() - 9);

                    }
                    //from form1
                    mwanachamaJinaKamili =intent.getStringExtra(KEY_FULLNAME_MWANACHAMA);
                    mwanachamaTareheKuzaliwa = intent.getStringExtra(KEY_DOB_MWANACHAMA);
                    mwanachamaNdoa = intent.getStringExtra(KEY_NDOA_MWANACHAMA);
                    mwanachamaJinsia = intent.getStringExtra(KEY_JINSIA_MWANACHAMA);
                    mwanachamaAnuani = intent.getStringExtra(KEY_ANUANI_MWANACHAMA);

                    //to form 3 for submission
                    Intent intent = new Intent(form2Activity.this,form3Activity.class);
                    intent.putExtra(KEY_FULLNAME_MWANACHAMA,mwanachamaJinaKamili);
                    intent.putExtra(KEY_DOB_MWANACHAMA,mwanachamaTareheKuzaliwa);
                    intent.putExtra(KEY_NDOA_MWANACHAMA,mwanachamaNdoa);
                    intent.putExtra(KEY_JINSIA_MWANACHAMA,mwanachamaJinsia);
                    intent.putExtra(KEY_ANUANI_MWANACHAMA,mwanachamaAnuani);
                    intent.putExtra(KEY_WILAYA_MWANACHAMA,mwanachamaWilaya);
                    intent.putExtra(KEY_MKOA_MWANACHAMA,mwanachamaMkoa);
                    intent.putExtra(KEY_SIMU_MWANACHAMA,mwanachamaSimu);
                    intent.putExtra(KEY_BARUA_MWANACHAMA,mwanachamaBaruaPepe);
                    intent.putExtra(KEY_USAJILI_MWANACHAMA,mwanachamaTareheUsajili);
                    startActivity(intent);


                } else {
                    Toast.makeText(form2Activity.this,
                            "One or more fields left empty!",
                            Toast.LENGTH_LONG).show();

                }
                          }
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
finish();
            }
        });
    }

    private void setToolBar(){
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    private String GetCountryZipCode(){
        String CountryID="";
        String CountryZipCode="";

        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }


    private void updateLabel() {
        String myFormat = "MM-dd-yyy";
        //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        mwanachamaTareheUsajiliEditText.setText(sdf.format(myCalendar.getTime()));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.logout:
                session.setLogin(false);
                Intent intent = new Intent(getBaseContext(), loginActivity.class);
                startActivity(intent);
                finish();
                return true;

            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
