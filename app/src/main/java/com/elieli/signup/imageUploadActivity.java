package com.elieli.signup;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static com.elieli.signup.form1Activity.KEY_DOB_MWANACHAMA;
import static com.elieli.signup.form1Activity.KEY_FULLNAME_MWANACHAMA;

public class imageUploadActivity extends AppCompatActivity {

    Button GetImageFromGalleryButton, UploadImageOnServerButton, GetIDImageFromGalleryButton, UploadIDImageOnServerButton, DoneButton;
    ImageView ShowSelectedImage, ShowSelectedIDImage;
    EditText imageName;
    Bitmap FixBitmap;
    public static final String KEY_ImageTag = "image_tag" ;
    public static final String KEY_ImageName = "image_data" ;
    ProgressDialog progressDialog ;
    ByteArrayOutputStream byteArrayOutputStream ;
    byte[] byteArray ;
    public String ConvertImage ;
    public String GetImageNameFromEditText;
    HttpURLConnection httpURLConnection ;
    URL url;
    OutputStream outputStream;
    BufferedWriter bufferedWriter ;
    int RC ;
    BufferedReader bufferedReader ;
    StringBuilder stringBuilder;
    boolean check = true;
    private int GALLERY = 1, CAMERA = 2;
    public int BUTTON_KEY = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_upload);

        GetImageFromGalleryButton = (Button)findViewById(R.id.buttonSelect);
        GetIDImageFromGalleryButton = (Button) findViewById(R.id.buttonSelectID);
        UploadImageOnServerButton = (Button)findViewById(R.id.buttonUpload);
        UploadIDImageOnServerButton = (Button)findViewById(R.id.buttonUploadID);
        DoneButton = (Button) findViewById(R.id.btn_done);
        ShowSelectedImage = (ImageView)findViewById(R.id.imageView);
        ShowSelectedIDImage = (ImageView) findViewById(R.id.imageViewID);

        DoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                String ConvertImage ;
//                String GetImageNameFromEditText;
                //to form 3 for submission
                Intent intent = new Intent(imageUploadActivity.this,form3Activity.class);
                intent.putExtra(KEY_ImageTag,GetImageNameFromEditText);
                intent.putExtra(KEY_ImageName,ConvertImage);
                //onBackPressed();
                //startActivity(intent);
            }
        });


        imageName=(EditText)findViewById(R.id.imageName);
        byteArrayOutputStream = new ByteArrayOutputStream();

        GetImageFromGalleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPictureDialog();
                BUTTON_KEY = 3;


            }
        });

        GetIDImageFromGalleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
                BUTTON_KEY = 4;

            }
        });


        UploadImageOnServerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GetImageNameFromEditText = imageName.getText().toString();
                //BUTTON_KEY = 5;
                UploadImageToServer();

            }
        });

        UploadIDImageOnServerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetImageNameFromEditText = imageName.getText().toString();
                //BUTTON_KEY = 6;
                UploadImageToServer();

            }
        });

        if (ContextCompat.checkSelfPermission(imageUploadActivity.this, android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                        5);
            }
        }
    }

    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Photo Gallery",
                "Camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    FixBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    // String path = saveImage(bitmap);
                    //Toast.makeText(MainActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    if(BUTTON_KEY == 3){
                        ShowSelectedImage.setImageBitmap(FixBitmap);
                        UploadImageOnServerButton.setVisibility(View.VISIBLE);
                    } else if (BUTTON_KEY == 4){
                        ShowSelectedIDImage.setImageBitmap(FixBitmap);
                        UploadIDImageOnServerButton.setVisibility(View.VISIBLE);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(imageUploadActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            FixBitmap = (Bitmap) data.getExtras().get("data");
            //ShowSelectedImage.setImageBitmap(FixBitmap);
            //UploadImageOnServerButton.setVisibility(View.VISIBLE);
            if(BUTTON_KEY == 3){
                ShowSelectedImage.setImageBitmap(FixBitmap);
                UploadImageOnServerButton.setVisibility(View.VISIBLE);
            } else if (BUTTON_KEY == 4){
                ShowSelectedIDImage.setImageBitmap(FixBitmap);
                UploadIDImageOnServerButton.setVisibility(View.VISIBLE);
            }
            //  saveImage(thumbnail);
            //Toast.makeText(ShadiRegistrationPart5.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }


    public void UploadImageToServer(){

        FixBitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);

        byteArray = byteArrayOutputStream.toByteArray();

        ConvertImage = Base64.encodeToString(byteArray, Base64.DEFAULT);

        class AsyncTaskUploadClass extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {

                super.onPreExecute();

                progressDialog = ProgressDialog.show(imageUploadActivity.this,"Image is Uploading","Please Wait",false,false);
            }

            @Override
            protected void onPostExecute(String string1) {

                super.onPostExecute(string1);

                progressDialog.dismiss();


                    Toast.makeText(imageUploadActivity.this,string1,Toast.LENGTH_LONG).show();





            }

            @Override
            protected String doInBackground(Void... params) {

                ImageProcessClass imageProcessClass = new ImageProcessClass();

                HashMap<String,String> HashMapParams = new HashMap<String,String>();

                HashMapParams.put(KEY_ImageTag, GetImageNameFromEditText);

                HashMapParams.put(KEY_ImageName, ConvertImage);

                String FinalData = imageProcessClass.ImageHttpRequest("http://smartdarasa.com/movies/imageUpload.php", HashMapParams);

                return FinalData;
            }
        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();
        AsyncTaskUploadClassOBJ.execute();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera

            }
            else {

                Toast.makeText(imageUploadActivity.this, "Unable to use Camera..Please Allow us to use Camera", Toast.LENGTH_LONG).show();

            }
        }
    }
}
