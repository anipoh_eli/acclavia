package com.elieli.signup;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class niaNjemaActivity extends AppCompatActivity {

    public static final String KEY_FULLNAME_MWANACHAMA = "mwanachama_jinaKamili";
    public static final String KEY_JINSIA_MWANACHAMA = "mwanachama_jinsia";
    public static final String KEY_DOB_MWANACHAMA = "mwanachama_tareheKuzaliwa";
    public static final String KEY_NDOA_MWANACHAMA = "mwanachama_haliNdoa";
    public static final String KEY_MWENZA_MWANACHAMA = "mwanachama_jinaMwenza";
    private static String STRING_EMPTY = "";
    private Toolbar toolbar;
    //private static final String KEY_UHUSIANO_MWANACHAMA = "mwanachama_uhusiano";
    //private static final String KEY_SIMU_MWANACHAMA = "mwanachama_simuNamba";

    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };




    public EditText mwanachamaJinaKamiliEditText;
    public EditText mwanachamaJinsiaEditText;
    public EditText mwanachamaTareheKuzaliwaEditText;
    public EditText mwanachamaNdoaEditText;
    public EditText mwanachamaMwenzaEditText;
    public Spinner mwanachamaJinsiaSpinner;
    public Spinner mwanachamaNdoaSpinner;
    public String mwanachamaJinaKamili;
    public String mwanachamaJinsia;
    public String mwanachamaTareheKuzaliwa;
    public String mwanachamaNdoa;
    public String mwanachamaMwenza;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nia_njema);
        setToolBar();
        session = new SessionManager(getApplicationContext());
        mwanachamaJinaKamiliEditText = (EditText) findViewById(R.id.mwanachamaJinaKamiliEditTextnia);
        mwanachamaJinsiaEditText = (EditText) findViewById(R.id.mwanachamaJinsiaEditTextnia);
        mwanachamaTareheKuzaliwaEditText = (EditText) findViewById(R.id.mwanachamaTareheKuzaliwaEditTextnia);
        mwanachamaNdoaEditText = (EditText) findViewById(R.id.mwanachamaNdoaEditTextnia);
        mwanachamaMwenzaEditText = (EditText) findViewById(R.id.mwanachamaMwenzaEditTextnia);
        //spinner object on form one
        mwanachamaJinsiaSpinner = (Spinner) findViewById(R.id.spinner_gender_nia);
        mwanachamaNdoaSpinner = (Spinner) findViewById(R.id.spinner_status_nia);

        //Gender spinner
        ArrayAdapter<CharSequence> genderAdpter = ArrayAdapter.createFromResource
                (this,R.array.array_gender_nia,R.layout.support_simple_spinner_dropdown_item);
        mwanachamaJinsiaSpinner.setAdapter(genderAdpter);
        mwanachamaJinsiaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mwanachamaJinsia = mwanachamaJinsiaSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //maritial status spinner
        ArrayAdapter<CharSequence> statusAdpter = ArrayAdapter.createFromResource
                (this,R.array.array_maritial_nia,R.layout.support_simple_spinner_dropdown_item);
        mwanachamaNdoaSpinner.setAdapter(statusAdpter);
        mwanachamaNdoaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mwanachamaNdoa = mwanachamaNdoaSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mwanachamaTareheKuzaliwaEditText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(niaNjemaActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });




        Button buttonNext1 =  (Button) findViewById(R.id.btn_next1_nia);
        buttonNext1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!STRING_EMPTY.equals(mwanachamaJinaKamiliEditText.getText().toString()) &&
                        !STRING_EMPTY.equals(mwanachamaTareheKuzaliwaEditText.getText().toString()) &&
                        STRING_EMPTY.equals(mwanachamaJinsiaEditText.getText().toString()) &&
                        STRING_EMPTY.equals(mwanachamaNdoaEditText.getText().toString())){

                    mwanachamaJinaKamili = mwanachamaJinaKamiliEditText.getText().toString();
                    //mwanachamaJinsia = mwanachamaJinsiaEditText.getText().toString();
                    mwanachamaJinsia = mwanachamaJinsiaSpinner.getSelectedItem().toString();
                    mwanachamaTareheKuzaliwa = mwanachamaTareheKuzaliwaEditText.getText().toString();
                    //mwanachamaNdoa = mwanachamaNdoaEditText.getText().toString();
                    mwanachamaNdoa = mwanachamaNdoaSpinner.getSelectedItem().toString();
                    mwanachamaMwenza = mwanachamaMwenzaEditText.getText().toString();

                    Intent intent = new Intent(niaNjemaActivity.this,niaNjema2Activity.class);
                    intent.putExtra(KEY_FULLNAME_MWANACHAMA,mwanachamaJinaKamili);
                    intent.putExtra(KEY_JINSIA_MWANACHAMA,mwanachamaJinsia);
                    intent.putExtra(KEY_DOB_MWANACHAMA,mwanachamaTareheKuzaliwa);
                    intent.putExtra(KEY_NDOA_MWANACHAMA,mwanachamaNdoa);
                    intent.putExtra(KEY_MWENZA_MWANACHAMA,mwanachamaMwenza);

                    startActivity(intent);


                    //views = viewsEditText.getText().toString();
                    //new form3Activity.ongezaMwanachamaAsyncTask().execute();
                } else {
                    Toast.makeText(niaNjemaActivity.this,
                            "One or more fields left empty!",
                            Toast.LENGTH_LONG).show();

                }
            }
        });

    }

    private void setToolBar(){
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
        private void updateLabel() {
            String myFormat = "MM-dd-yyy";
            //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            mwanachamaTareheKuzaliwaEditText.setText(sdf.format(myCalendar.getTime()));
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.logout:
                session.setLogin(false);
                Intent intent = new Intent(getBaseContext(), loginActivity.class);
                startActivity(intent);
                finish();
                return true;

            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
