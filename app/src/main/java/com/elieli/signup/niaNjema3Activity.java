package com.elieli.signup;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.elieli.signup.Helper.CheckNetworkStatus;
import com.elieli.signup.Helper.HttpJsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.elieli.signup.niaNjema2Activity.KEY_ANUANI_MWANACHAMA;
import static com.elieli.signup.niaNjema2Activity.KEY_BARUA_MWANACHAMA;
import static com.elieli.signup.niaNjema2Activity.KEY_DINI_MWANACHAMA;
import static com.elieli.signup.niaNjema2Activity.KEY_MTAA_MWANACHAMA;
import static com.elieli.signup.niaNjema2Activity.KEY_SIMU_MWANACHAMA;
import static com.elieli.signup.niaNjemaActivity.KEY_DOB_MWANACHAMA;
import static com.elieli.signup.niaNjemaActivity.KEY_FULLNAME_MWANACHAMA;
import static com.elieli.signup.niaNjemaActivity.KEY_JINSIA_MWANACHAMA;
import static com.elieli.signup.niaNjemaActivity.KEY_MWENZA_MWANACHAMA;
import static com.elieli.signup.niaNjemaActivity.KEY_NDOA_MWANACHAMA;

public class niaNjema3Activity extends AppCompatActivity implements View.OnClickListener {

    private static final String KEY_SUCCESS = "success";
    private static final String KEY_PLAN_MTEGEMEZI_NIA = "beneficiary_Plan";
    private static final String KEY_FULLNAME_MTEGEMEZI_NIA = "beneficiary_fullName";
    private static final String KEY_SIMU_MTEGEMEZI_NIA = "beneficiary_Phone";
    private static final String KEY_DOB_MTEGEMEZI_NIA = "beneficiary_DOB";
    private static final String KEY_ADRESS_MTEGEMEZI_NIA = "beneficiary_Adress";

    private static final String KEY_CHILD_FULLNAME = "child_fullName";
    private static final String KEY_CHILD_DOB = "child_DOB";
    private static final String KEY_CHILD_FULLNAME02 = "child_fullName02";
    private static final String KEY_CHILD_DOB02 = "child_DOB02";
    private static final String KEY_CHILD_FULLNAME03 = "child_fullName03";
    private static final String KEY_CHILD_DOB03 = "child_DOB03";
    private static final String KEY_CHILD_FULLNAME04 = "child_fullName04";
    private static final String KEY_CHILD_DOB04 = "child_DOB04";
    // private static final String KEY_VIEWS = "views";
    private static final String BASE_URL = "http://smartdarasa.com/movies/";
    private static String STRING_EMPTY = "";
    private EditText beneficiaryFullNameEditText,beneficiaryChildNameEditText,beneficiaryChildNameEditText2, beneficiaryChildNameEditText3, beneficiaryChildNameEditText4;
    private EditText beneficiaryPlanEditText;
    private EditText beneficiaryPhoneEditText;
    private EditText beneficiaryDobEditText,beneficiaryDobEditText2,beneficiaryDobEditText3,beneficiaryDobEditText4,beneficiaryDobEditText5;
    private EditText beneficiaryAdressEditText;
    private Spinner planSpinner;
    public String mwanachamaJinaKamili;
    public String mwanachamaJinsia;
    public String mwanachamaTareheKuzaliwa;
    public String mwanachamaNdoa;
    public String mwanachamaMwenza;
    public String mwanachamaDini;
    public String mwanachamaBarua;
    public String mwanachamaSimu;
    public String mwanachamaAnuani;
    public String mwanachamaMtaa;
    public String beneficiaryPlan;
    public String beneficiaryFullName,beneficiaryChildName,beneficiaryChildName2,beneficiaryChildName3,beneficiaryChildName4;
    public String beneficiaryPhone;
    public String beneficiaryDob,beneficiaryDob2,beneficiaryDob3,beneficiaryDob4,beneficiaryDob5;
    public String beneficiaryAdress;
    private Calendar startDate;
    private Calendar endDate;
    private Calendar activeDate;
    static final int DATE_mtegemezi_ID = 0;
    static final int DATE_mtegemezi2_ID = 1;
    static final int DATE_mtegemezi3_ID = 2;
    static final int DATE_mtegemezi4_ID = 3;
    static final int DATE_mtegemezi5_ID = 4;
    private EditText activeDateDisplay;
    //private String views;
    private Button submitButton;
    private int success;
    private ProgressDialog pDialog;
    private Toolbar toolbar;
    private  SessionManager session;
    private  String member_id;

    //form image activity ------------------------
    Button GetImageFromGalleryButton, UploadImageOnServerButton, GetIDImageFromGalleryButton, UploadIDImageOnServerButton, DoneButton;
    ImageView ShowSelectedImage, ShowSelectedIDImage;
    EditText imageName;
    Bitmap FixBitmap,FixBitmapIDC;
    public static final String KEY_ImageTag_Nia = "image_tag" ;
    public static final String KEY_ImageString_Nia = "image_data" ;
    public static final String KEY_ImageStringIDC_Nia = "image_data_idc" ;

    ProgressDialog progressDialog ;
    ByteArrayOutputStream byteArrayOutputStream,byteArrayOutputStreamIDC;
    byte[] byteArray,byteArrayIDC;
    public String ConvertImage,ConvertImageIDC;
    public String GetImageNameFromEditText;
    private int GALLERY = 1, CAMERA = 2;
    public int BUTTON_KEY = 0;
    //---------------------------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nia_njema3);
        session = new SessionManager(getApplicationContext());
        setToolBar();


        beneficiaryPlanEditText = (EditText) findViewById(R.id.beneficiaryPlanEditText);
        beneficiaryFullNameEditText = (EditText) findViewById(R.id.beneficiaryFullNameEditText);
        beneficiaryPhoneEditText = (EditText) findViewById(R.id.beneficiaryPhoneEditText);
        beneficiaryDobEditText = (EditText) findViewById(R.id.beneficiaryDobEditText);
        beneficiaryAdressEditText = (EditText) findViewById(R.id.beneficiaryAdressEditText);
        planSpinner = (Spinner) findViewById(R.id.spinner_plan);
        //child 1
        beneficiaryChildNameEditText = (EditText) findViewById(R.id.beneficiaryFullNameEditText2);
        beneficiaryDobEditText2 = (EditText) findViewById(R.id.beneficiaryDobEditText2);
        //child 2
        beneficiaryChildNameEditText2 = (EditText) findViewById(R.id.beneficiaryFullNameEditText3);
        beneficiaryDobEditText3 = (EditText) findViewById(R.id.beneficiaryDobEditText3);
        //child 3
        beneficiaryChildNameEditText3 = (EditText) findViewById(R.id.beneficiaryFullNameEditText4);
        beneficiaryDobEditText4 = (EditText) findViewById(R.id.beneficiaryDobEditText4);
        //child 4
        beneficiaryChildNameEditText4 = (EditText) findViewById(R.id.beneficiaryFullNameEditText5);
        beneficiaryDobEditText5 = (EditText) findViewById(R.id.beneficiaryDobEditText5);


        //form image activity ------------------------
        GetImageFromGalleryButton = (Button)findViewById(R.id.buttonSelectNia);
        GetIDImageFromGalleryButton = (Button) findViewById(R.id.buttonSelectIDNia);
        UploadImageOnServerButton = (Button)findViewById(R.id.buttonUploadNia);
        UploadIDImageOnServerButton = (Button)findViewById(R.id.buttonUploadIDNia);
        //DoneButton = (Button) findViewById(R.id.btn_done);
        ShowSelectedImage = (ImageView)findViewById(R.id.imageViewNia);
        ShowSelectedIDImage = (ImageView) findViewById(R.id.imageViewIDNia);

        //get from form 1
        final Intent intent = getIntent();
        mwanachamaJinaKamili = intent.getStringExtra(KEY_FULLNAME_MWANACHAMA);
        mwanachamaJinsia = intent.getStringExtra(KEY_JINSIA_MWANACHAMA);
        mwanachamaTareheKuzaliwa = intent.getStringExtra(KEY_DOB_MWANACHAMA);
        mwanachamaNdoa = intent.getStringExtra(KEY_NDOA_MWANACHAMA);
        mwanachamaMwenza = intent.getStringExtra(KEY_MWENZA_MWANACHAMA);
        //get form 2
        mwanachamaDini = intent.getStringExtra(KEY_DINI_MWANACHAMA);
        mwanachamaBarua = intent.getStringExtra(KEY_BARUA_MWANACHAMA);
        mwanachamaSimu = intent.getStringExtra(KEY_SIMU_MWANACHAMA);
        mwanachamaAnuani = intent.getStringExtra(KEY_ANUANI_MWANACHAMA);
        mwanachamaMtaa = intent.getStringExtra(KEY_MTAA_MWANACHAMA);
        member_id= utils.readPreferences(niaNjema3Activity.this,"member_id","");

        submitButton = (Button) findViewById(R.id.btn_submit_nia);

        Button buttonBack = (Button) findViewById(R.id.btn_backInsubmitNia);

        Button picUpload = (Button) findViewById(R.id.button_wekaPicha_nia);

        /* get the current date */
        startDate = Calendar.getInstance();
        /* get the current date */
        endDate = Calendar.getInstance();

        beneficiaryDobEditText.setOnClickListener(this);
        beneficiaryDobEditText2.setOnClickListener(this);
        beneficiaryDobEditText3.setOnClickListener(this);
        beneficiaryDobEditText4.setOnClickListener(this);
        beneficiaryDobEditText5.setOnClickListener(this);
        buttonBack.setOnClickListener(this);
        picUpload.setOnClickListener(this);
        submitButton.setOnClickListener(this);



        //Plan spinner
        ArrayAdapter<CharSequence> genderAdpter = ArrayAdapter.createFromResource
                (this,R.array.array_plan,R.layout.support_simple_spinner_dropdown_item);
        planSpinner.setAdapter(genderAdpter);
        planSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                beneficiaryPlan = planSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //--------------------------------------------------------
        imageName=(EditText)findViewById(R.id.imageName);
        byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStreamIDC = new ByteArrayOutputStream();


        GetImageFromGalleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPictureDialog();
                BUTTON_KEY = 3;


            }
        });

        GetIDImageFromGalleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
                BUTTON_KEY = 4;

            }
        });

//
//        UploadImageOnServerButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                GetImageNameFromEditText = imageName.getText().toString();
//                //BUTTON_KEY = 5;
//                UploadImageToServer();
//
//            }
//        });
//
//        UploadIDImageOnServerButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                GetImageNameFromEditText = imageName.getText().toString();
//                //BUTTON_KEY = 6;
//                UploadImageToServer();
//
//            }
//        });

        if (ContextCompat.checkSelfPermission(niaNjema3Activity.this, android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                        5);
            }
        }

        //-----------------------------------------------------------

    }

    private void setToolBar(){
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

@Override
public void onClick(View v) {
        switch (v.getId()){

        case R.id.beneficiaryDobEditText:

        showDateDialog(beneficiaryDobEditText, startDate);
        break;
        case R.id.beneficiaryDobEditText2:

        showDateDialog(beneficiaryDobEditText2, startDate);
        break;
        case R.id.beneficiaryDobEditText3:

        showDateDialog(beneficiaryDobEditText3, startDate);
        break;

        case R.id.beneficiaryDobEditText4:

                showDateDialog(beneficiaryDobEditText4, startDate);
                break;
        case R.id.beneficiaryDobEditText5:

                showDateDialog(beneficiaryDobEditText5, startDate);
                break;
        case R.id.btn_submit_nia:

        if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {

        ongezaMwanachama();

        Toast.makeText(getApplicationContext(),"Data submitted",Toast.LENGTH_LONG).show();
//                Intent intent = new Intent(form3Activity.this,selectActivity.class);
//                startActivity(intent);
        } else {
        Toast.makeText(niaNjema3Activity.this,
        "Unable to connect to internet",
        Toast.LENGTH_LONG).show();
        return;

        }

        break;
        case R.id.btn_backInsubmitNia:
            this.finish();

        break;
        case R.id.button_wekaPicha_nia:
        Intent picIntent = new Intent(niaNjema3Activity.this,imageUploadActivity.class);
        startActivity(picIntent);
        break;


        }

        }

    //TODO make ongezaMwanachama to process form field and picture submission, STATUS:DONE
    private void ongezaMwanachama() {

        //--------------------------------------------
        FixBitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
        FixBitmapIDC.compress(Bitmap.CompressFormat.JPEG,40, byteArrayOutputStreamIDC);

        byteArray = byteArrayOutputStream.toByteArray();
        byteArrayIDC = byteArrayOutputStreamIDC.toByteArray();


        ConvertImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
        ConvertImageIDC = Base64.encodeToString(byteArrayIDC, Base64.DEFAULT);

//        Toast.makeText(form3Activity.this,
//                ConvertImage,
//                Toast.LENGTH_LONG).show();
        //--------------------------------------------

        if (STRING_EMPTY.equals(beneficiaryPlanEditText.getText().toString()) &&
                !STRING_EMPTY.equals(beneficiaryFullNameEditText.getText().toString()) &&
                !STRING_EMPTY.equals(beneficiaryPhoneEditText.getText().toString()) &&
                !STRING_EMPTY.equals(beneficiaryDobEditText.getText().toString())
                &&!STRING_EMPTY.equals(beneficiaryAdressEditText.getText().toString())){

            //beneficiaryPlan = beneficiaryPlanEditText.getText().toString();
            beneficiaryPlan = planSpinner.getSelectedItem().toString();
            beneficiaryFullName = beneficiaryFullNameEditText.getText().toString();
            beneficiaryPhone = beneficiaryPhoneEditText.getText().toString();
            beneficiaryDob = beneficiaryDobEditText.getText().toString();
            beneficiaryAdress = beneficiaryAdressEditText.getText().toString();
            beneficiaryChildName = beneficiaryChildNameEditText.getText().toString();
            beneficiaryDob2 = beneficiaryDobEditText2.getText().toString();
            beneficiaryChildName2 = beneficiaryChildNameEditText2.getText().toString();
            beneficiaryDob3 = beneficiaryDobEditText3.getText().toString();
            beneficiaryChildName3 = beneficiaryChildNameEditText3.getText().toString();
            beneficiaryDob4 = beneficiaryDobEditText4.getText().toString();
            beneficiaryChildName4 = beneficiaryChildNameEditText4.getText().toString();
            beneficiaryDob5 = beneficiaryDobEditText5.getText().toString();

             Log.d("TAG", beneficiaryDob+'-'+beneficiaryDob2+'-'+beneficiaryDob3);


            //views = viewsEditText.getText().toString();
            new ongezaMwanachamaAsyncTask().execute();
        } else {
            Toast.makeText(niaNjema3Activity.this,
                    "One or more fields left empty!",
                    Toast.LENGTH_LONG).show();

        }
    }

    private class ongezaMwanachamaAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display proggress bar
            pDialog = new ProgressDialog(niaNjema3Activity.this);
            pDialog.setMessage("Mwanachama anaongezwa. subiri...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpJsonParser httpJsonParser = new HttpJsonParser();
            Map<String, String> httpParams = new HashMap<>();
            //Populating request parameters

            httpParams.put(KEY_FULLNAME_MWANACHAMA, mwanachamaJinaKamili);
            httpParams.put(KEY_JINSIA_MWANACHAMA, mwanachamaJinsia);
            httpParams.put(KEY_DOB_MWANACHAMA, mwanachamaTareheKuzaliwa);
            httpParams.put(KEY_NDOA_MWANACHAMA, mwanachamaNdoa);
            httpParams.put(KEY_MWENZA_MWANACHAMA,mwanachamaMwenza);
            httpParams.put(KEY_DINI_MWANACHAMA,mwanachamaDini);
            httpParams.put(KEY_BARUA_MWANACHAMA,mwanachamaBarua);
            httpParams.put(KEY_SIMU_MWANACHAMA,mwanachamaSimu);
            httpParams.put(KEY_ANUANI_MWANACHAMA,mwanachamaAnuani);
            httpParams.put(KEY_MTAA_MWANACHAMA,mwanachamaMtaa);
            httpParams.put(KEY_PLAN_MTEGEMEZI_NIA,beneficiaryPlan);
            httpParams.put(KEY_FULLNAME_MTEGEMEZI_NIA,beneficiaryFullName);
            httpParams.put(KEY_SIMU_MTEGEMEZI_NIA,beneficiaryPhone);
            httpParams.put(KEY_DOB_MTEGEMEZI_NIA,beneficiaryDob);
            httpParams.put(KEY_ADRESS_MTEGEMEZI_NIA,beneficiaryAdress);

            httpParams.put(KEY_CHILD_FULLNAME,beneficiaryChildName);
            httpParams.put(KEY_CHILD_DOB,beneficiaryDob2);
            httpParams.put(KEY_CHILD_FULLNAME02,beneficiaryChildName2);
            httpParams.put(KEY_CHILD_DOB02,beneficiaryDob3);
            httpParams.put(KEY_CHILD_FULLNAME03,beneficiaryChildName3);
            httpParams.put(KEY_CHILD_DOB03,beneficiaryDob4);
            httpParams.put(KEY_CHILD_FULLNAME04,beneficiaryChildName4);
            httpParams.put(KEY_CHILD_DOB04,beneficiaryDob5);
            httpParams.put("status","1");
            httpParams.put("member_id",member_id);

            httpParams.put(KEY_ImageTag_Nia, GetImageNameFromEditText); // TODO: pass imageString to server STATUS:DONE
            httpParams.put(KEY_ImageString_Nia, ConvertImage); // pass picture image string
            httpParams.put(KEY_ImageStringIDC_Nia, ConvertImageIDC); // pass identification card string

            //httpParams.put(KEY_VIEWS, views);
            JSONObject jsonObject = httpJsonParser.makeHttpRequest(
                    BASE_URL + "ongeza_mwanachama_nia.php", "POST", httpParams);
            try {
                success = jsonObject.getInt(KEY_SUCCESS);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    if (success == 1) {
                        //Display success message
                        Toast.makeText(niaNjema3Activity.this,
                                "Mwanachama Ameongezwa", Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(getApplicationContext(), selectActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }else if (success == 0){

                        Toast.makeText(niaNjema3Activity.this,
                                "Error while adding (binding)", Toast.LENGTH_LONG).show();
                    } else if (success == 2){
                        Toast.makeText(niaNjema3Activity.this,
                                "Mandatory parameter missing", Toast.LENGTH_LONG).show();
                    } else if (success == 3){
                        Toast.makeText(niaNjema3Activity.this,
                                "Error in inserting", Toast.LENGTH_LONG).show();
                    }
//                    else {
//                        Toast.makeText(AddMovieActivity.this,
//                                "Some error occurred while adding movie",
//                                Toast.LENGTH_LONG).show();
//
//                    }
                }
            });
        }
    }



    private void updateDisplay(EditText dateDisplay, Calendar date) {
        dateDisplay.setText(
                new StringBuilder()
                        // Month is 0 based so add 1
                        .append(date.get(Calendar.MONTH) + 1).append("-")
                        .append(date.get(Calendar.DAY_OF_MONTH)).append("-")
                        .append(date.get(Calendar.YEAR)).append(" "));

    }

    public void showDateDialog(EditText dateDisplay, Calendar date) {
        activeDateDisplay = dateDisplay;
        activeDate = date;
        showDialog(DATE_mtegemezi_ID);
    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            activeDate.set(Calendar.YEAR, year);
            activeDate.set(Calendar.MONTH, monthOfYear);
            activeDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDisplay(activeDateDisplay, activeDate);
            unregisterDateDisplay();
        }
    };

    private void unregisterDateDisplay() {
        activeDateDisplay = null;
        activeDate = null;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_mtegemezi_ID:
                return new DatePickerDialog(this, dateSetListener, activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
            case DATE_mtegemezi2_ID:
                return new DatePickerDialog(this, dateSetListener, activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
            case DATE_mtegemezi3_ID:
                return new DatePickerDialog(this, dateSetListener, activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
            case DATE_mtegemezi4_ID:
                return new DatePickerDialog(this, dateSetListener, activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
            case DATE_mtegemezi5_ID:
                return new DatePickerDialog(this, dateSetListener, activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));

        }
        return null;
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        switch (id) {
            case DATE_mtegemezi_ID:
                ((DatePickerDialog) dialog).updateDate(activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
                break;
            case DATE_mtegemezi2_ID:
                ((DatePickerDialog) dialog).updateDate(activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
                break;
            case DATE_mtegemezi3_ID:
                ((DatePickerDialog) dialog).updateDate(activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
                break;
            case DATE_mtegemezi4_ID:
                ((DatePickerDialog) dialog).updateDate(activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
                break;
            case DATE_mtegemezi5_ID:
                ((DatePickerDialog) dialog).updateDate(activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
                break;
        }
    }

    //------------------------------

    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Photo Gallery",
                "Camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    FixBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    FixBitmapIDC = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    // String path = saveImage(bitmap);
                    //Toast.makeText(MainActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    if(BUTTON_KEY == 3){
                        ShowSelectedImage.setImageBitmap(FixBitmap);
                        //UploadImageOnServerButton.setVisibility(View.VISIBLE);
                    } else if (BUTTON_KEY == 4){
                        ShowSelectedIDImage.setImageBitmap(FixBitmapIDC);
                        //UploadIDImageOnServerButton.setVisibility(View.VISIBLE);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(niaNjema3Activity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            FixBitmap = (Bitmap) data.getExtras().get("data");
            FixBitmapIDC = (Bitmap) data.getExtras().get("data");
            //ShowSelectedImage.setImageBitmap(FixBitmap);
            //UploadImageOnServerButton.setVisibility(View.VISIBLE);
            if(BUTTON_KEY == 3){
                ShowSelectedImage.setImageBitmap(FixBitmap);
                //UploadImageOnServerButton.setVisibility(View.VISIBLE);
            } else if (BUTTON_KEY == 4){
                ShowSelectedIDImage.setImageBitmap(FixBitmapIDC);
                //UploadIDImageOnServerButton.setVisibility(View.VISIBLE);
            }
            //  saveImage(thumbnail);
            //Toast.makeText(ShadiRegistrationPart5.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera

            }
            else {

                Toast.makeText(niaNjema3Activity.this, "Unable to use Camera..Please Allow us to use Camera", Toast.LENGTH_LONG).show();

            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.logout:
                session.setLogin(false);
                Intent intent = new Intent(getBaseContext(), loginActivity.class);
                startActivity(intent);
                finish();
                return true;
            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
