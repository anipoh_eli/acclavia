package com.elieli.signup;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import static com.elieli.signup.niaNjemaActivity.KEY_DOB_MWANACHAMA;
import static com.elieli.signup.niaNjemaActivity.KEY_FULLNAME_MWANACHAMA;
import static com.elieli.signup.niaNjemaActivity.KEY_JINSIA_MWANACHAMA;
import static com.elieli.signup.niaNjemaActivity.KEY_MWENZA_MWANACHAMA;
import static com.elieli.signup.niaNjemaActivity.KEY_NDOA_MWANACHAMA;

public class niaNjema2Activity extends AppCompatActivity {

    public static final String KEY_DINI_MWANACHAMA = "mwanachama_dini";
    public static final String KEY_BARUA_MWANACHAMA = "mwanachama_baruaPepe";
    public static final String KEY_SIMU_MWANACHAMA = "mwanachama_simu";
    public static final String KEY_ANUANI_MWANACHAMA = "mwanachama_anuani";
    public static final String KEY_MTAA_MWANACHAMA = "mwanachama_mtaa";
    private static String STRING_EMPTY = "";
    //private static final String KEY_UHUSIANO_MWANACHAMA = "mwanachama_uhusiano";
    //private static final String KEY_SIMU_MWANACHAMA = "mwanachama_simuNamba";
    public EditText mwanachamaDiniEditText;
    public EditText mwanachamaBaruaEditText;
    public EditText mwanachamaSimuEditText;
    public EditText mwanachamaAnuaniEditText;
    public EditText mwanachamaMtaaEditText;
    public Spinner mwanachamaDiniSpinner;
    public String mwanachamaJinaKamili;
    public String mwanachamaJinsia;
    public String mwanachamaTareheKuzaliwa;
    public String mwanachamaNdoa;
    public String mwanachamaMwenza;
    public String mwanachamaDini;
    public String mwanachamaBarua;
    public String mwanachamaSimu;
    public String mwanachamaAnuani;
    public String mwanachamaMtaa;
    private String validphone;
    private Toolbar toolbar;
    private  SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nia_njema2);
setToolBar();
        session = new SessionManager(getApplicationContext());
        Button picUpload = (Button) findViewById(R.id.button_wekaPicha_nia);

        mwanachamaDiniEditText = (EditText) findViewById(R.id.mwanachamaDiniEditTextnia);
        mwanachamaBaruaEditText = (EditText) findViewById(R.id.mwanachamaBaruaEditTextnia);
        mwanachamaSimuEditText = (EditText) findViewById(R.id.mwanachamaSimuEditTextnia);
        //mwanachamaNdoaSpinner = (Spinner) findViewById(R.id.spinner_marriage);
        mwanachamaAnuaniEditText = (EditText) findViewById(R.id.mwanachamaAnuaniEditTextnia);
        mwanachamaMtaaEditText = (EditText) findViewById(R.id.mwanachamaMtaaEditTextnia);
        mwanachamaDiniSpinner = (Spinner) findViewById(R.id.spinner_religion);

        //Gender spinner
        ArrayAdapter<CharSequence> diniAdpter = ArrayAdapter.createFromResource
                (this,R.array.array_religion,R.layout.support_simple_spinner_dropdown_item);
        mwanachamaDiniSpinner.setAdapter(diniAdpter);
        mwanachamaDiniSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mwanachamaDini = mwanachamaDiniSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //get from previos form
        final Intent intent = getIntent();
        mwanachamaJinaKamili = intent.getStringExtra(KEY_FULLNAME_MWANACHAMA);
        mwanachamaJinsia = intent.getStringExtra(KEY_JINSIA_MWANACHAMA);
        mwanachamaTareheKuzaliwa = intent.getStringExtra(KEY_DOB_MWANACHAMA);
        mwanachamaNdoa = intent.getStringExtra(KEY_NDOA_MWANACHAMA);
        mwanachamaMwenza = intent.getStringExtra(KEY_MWENZA_MWANACHAMA);


        Button buttonNext2 = (Button) findViewById(R.id.btn_next2);
        Button buttonBack = (Button) findViewById(R.id.btn_back2nia);
        buttonNext2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (STRING_EMPTY.equals(mwanachamaDiniEditText.getText().toString()) &&
                        !STRING_EMPTY.equals(mwanachamaBaruaEditText.getText().toString()) &&
                        !STRING_EMPTY.equals(mwanachamaSimuEditText.getText().toString()) &&
                        !STRING_EMPTY.equals(mwanachamaAnuaniEditText.getText().toString())&&
                        !STRING_EMPTY.equals(mwanachamaMtaaEditText.getText().toString())){

                    //form2 fields
                    //mwanachamaDini = mwanachamaDiniEditText.getText().toString();
                    mwanachamaDini = mwanachamaDiniSpinner.getSelectedItem().toString();
                    mwanachamaBarua = mwanachamaBaruaEditText.getText().toString();
                    validphone = mwanachamaSimuEditText.getText().toString();
                    mwanachamaAnuani = mwanachamaAnuaniEditText.getText().toString();
                    mwanachamaMtaa = mwanachamaMtaaEditText.getText().toString();


                    if(validphone.length() < 10 || validphone.length() > 14) {

                        Snackbar.make(v, "Please enter valid phonenumber!", Snackbar.LENGTH_LONG)
                                .show();
                        return;
                    }

                    if(!utils.isValidEmail(mwanachamaBarua)){
                        Snackbar.make(v, "Please Enter a valid email " , Snackbar
                                .LENGTH_LONG)
                                .show();
                        return;
                    }
                    else {
                        mwanachamaSimu = GetCountryZipCode() + validphone.substring(validphone.length() - 9);

                    }

                    //from form1
                    mwanachamaJinaKamili = intent.getStringExtra(KEY_FULLNAME_MWANACHAMA);
                    mwanachamaJinsia = intent.getStringExtra(KEY_JINSIA_MWANACHAMA);
                    mwanachamaTareheKuzaliwa = intent.getStringExtra(KEY_DOB_MWANACHAMA);
                    mwanachamaNdoa = intent.getStringExtra(KEY_NDOA_MWANACHAMA);
                    mwanachamaMwenza = intent.getStringExtra(KEY_MWENZA_MWANACHAMA);


                    //to form 3 for submission
                    Intent intent = new Intent(niaNjema2Activity.this,niaNjema3Activity.class);
                    intent.putExtra(KEY_FULLNAME_MWANACHAMA,mwanachamaJinaKamili);
                    intent.putExtra(KEY_JINSIA_MWANACHAMA,mwanachamaJinsia);
                    intent.putExtra(KEY_DOB_MWANACHAMA,mwanachamaTareheKuzaliwa);
                    intent.putExtra(KEY_NDOA_MWANACHAMA,mwanachamaNdoa);
                    intent.putExtra(KEY_MWENZA_MWANACHAMA,mwanachamaMwenza);
                    intent.putExtra(KEY_DINI_MWANACHAMA,mwanachamaDini);
                    intent.putExtra(KEY_BARUA_MWANACHAMA,mwanachamaBarua);
                    intent.putExtra(KEY_SIMU_MWANACHAMA,mwanachamaSimu);
                    intent.putExtra(KEY_ANUANI_MWANACHAMA,mwanachamaAnuani);
                    intent.putExtra(KEY_MTAA_MWANACHAMA,mwanachamaMtaa);
                    startActivity(intent);


                } else {
                    Toast.makeText(niaNjema2Activity.this,
                            "One or more fields left empty!",
                            Toast.LENGTH_LONG).show();

                }
            }
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(niaNjema2Activity.this,niaNjemaActivity.class);
                //startActivity(intent);
                finish();
            }
        });

    }

    private void setToolBar(){
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    private String GetCountryZipCode(){
        String CountryID="";
        String CountryZipCode="";

        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.logout:
                session.setLogin(false);
                Intent intent = new Intent(getBaseContext(), loginActivity.class);
                startActivity(intent);
                finish();
                return true;
            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
