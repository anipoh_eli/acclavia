package com.elieli.signup;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import android.app.DatePickerDialog;

import com.elieli.signup.Helper.CheckNetworkStatus;
import com.elieli.signup.Helper.HttpJsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.elieli.signup.form1Activity.KEY_ANUANI_MWANACHAMA;
import static com.elieli.signup.form1Activity.KEY_DOB_MWANACHAMA;
import static com.elieli.signup.form1Activity.KEY_FULLNAME_MWANACHAMA;
import static com.elieli.signup.form1Activity.KEY_JINSIA_MWANACHAMA;
import static com.elieli.signup.form1Activity.KEY_NDOA_MWANACHAMA;
import static com.elieli.signup.form2Activity.KEY_BARUA_MWANACHAMA;
import static com.elieli.signup.form2Activity.KEY_MKOA_MWANACHAMA;
import static com.elieli.signup.form2Activity.KEY_SIMU_MWANACHAMA;
import static com.elieli.signup.form2Activity.KEY_USAJILI_MWANACHAMA;
import static com.elieli.signup.form2Activity.KEY_WILAYA_MWANACHAMA;

public class form3Activity extends AppCompatActivity implements View.OnClickListener {

    private static final String KEY_SUCCESS = "success";
    private static final String KEY_FULLNAME_MTEGEMEZI = "mtegemezi_jinaKamili";
    private static final String KEY_DOB_MTEGEMEZI = "mtegemezi_tareheKuzaliwa";
    private static final String KEY_JINSIA_MTEGEMEZI = "mtegemezi_jinsia";
    private static final String KEY_UHUSIANO_MTEGEMEZI = "mtegemezi_uhusiano";
    private static final String KEY_SIMU_MTEGEMEZI = "mtegemezi_simuNamba";

    private static final String KEY_FULLNAME_MTEGEMEZI_WAPILI = "mtegemezi_jinaKamili_wapili";
    private static final String KEY_DOB_MTEGEMEZI_WAPILI = "mtegemezi_tareheKuzaliwa_wapili";
    private static final String KEY_JINSIA_MTEGEMEZI_WAPILI = "mtegemezi_jinsia_wapili";
    private static final String KEY_UHUSIANO_MTEGEMEZI_WAPILI = "mtegemezi_uhusiano_wapili";
    private static final String KEY_SIMU_MTEGEMEZI_WAPILI = "mtegemezi_simuNamba_wapili";

    private static final String KEY_FULLNAME_MTEGEMEZI_WATATU = "mtegemezi_jinaKamili_watatu";
    private static final String KEY_DOB_MTEGEMEZI_WATATU = "mtegemezi_tareheKuzaliwa_watatu";
    private static final String KEY_JINSIA_MTEGEMEZI_WATATU = "mtegemezi_jinsia_watatu";
    private static final String KEY_UHUSIANO_MTEGEMEZI_WATATU = "mtegemezi_uhusiano_watatu";
    private static final String KEY_SIMU_MTEGEMEZI_WATATU = "mtegemezi_simuNamba_watatu";
   // private static final String KEY_VIEWS = "views";

    private static final String BASE_URL = "http://smartdarasa.com/movies/";
    private static String STRING_EMPTY = "";
    private EditText mtegemeziJinaKamiliEditText,mtegemeziJinaKamiliEditText2,mtegemeziJinaKamiliEditText3;
    private EditText mtegemeziTareheKuzaliwaEditText,mtegemeziTareheKuzaliwaEditText2,mtegemeziTareheKuzaliwaEditText3;
    private EditText mtegemeziJinsiaEditText;
    private EditText mtegemeziUhusianoEditText, mtegemeziUhusianoEditText2,mtegemeziUhusianoEditText3;
    private EditText mtegemeziSimuNambaEditText, mtegemeziSimuNambaEditText2,mtegemeziSimuNambaEditText3;
    private Spinner mtegemeziGenderSpinner, mtegemeziGenderSpinner01, mtegemeziGenderSpinner02;
    //private EditText viewsEditText;
    private String mtegemeziJinaKamili,mtegemeziJinaKamili2,mtegemeziJinaKamili3;
    private String mtegemeziTareheKuzaliwa,mtegemeziTareheKuzaliwa2,mtegemeziTareheKuzaliwa3;
    private String mtegemeziJinsia,mtegemeziJinsia2,mtegemeziJinsia3;
    private String mtegemeziUhusiano,mtegemeziUhusiano2,mtegemeziUhusiano3;
    private String mtegemeziSimuNamba,mtegemeziSimuNamba2,mtegemeziSimuNamba3;
    private String mwanachamaJinaKamili;
    private String mwanachamaTareheKuzaliwa;
    private String mwanachamaNdoa;
    private String mwanachamaJinsia;
    private String mwanachamaAnuani;
    public String mwanachamaWilaya;
    public String mwanachamaMkoa;
    public String mwanachamaSimu;
    public String mwanachamaBaruaPepe;
    public String mwanachamaTareheUsajili;
   // public String ConvertImage ;
    //public String GetImageNameFromEditText;
    private Calendar startDate;
    private Calendar endDate;
    private Calendar activeDate;
    static final int DATE_mtegemezi_ID = 0;
    static final int DATE_mtegemezi2_ID = 1;
    static final int DATE_mtegemezi3_ID = 2;
    private Toolbar toolbar;
    private  SessionManager session;
    private  String member_id;

    private EditText activeDateDisplay;

    //private String views;
    private Button submitButton;
    private int success;
    private ProgressDialog pDialog;

    //form image activity ------------------------
    Button GetImageFromGalleryButton, UploadImageOnServerButton, GetIDImageFromGalleryButton, UploadIDImageOnServerButton, DoneButton;
    ImageView ShowSelectedImage, ShowSelectedIDImage;
    EditText imageName;
    Bitmap FixBitmap, FixBitmapIDC;
    public static final String KEY_ImageTag = "image_tag" ;
    public static final String KEY_ImageName = "image_data" ;
    public static final String KEY_ImageStringIDC = "image_data_idc" ;

    ProgressDialog progressDialog ;
    ByteArrayOutputStream byteArrayOutputStream, byteArrayOutputStreamIDC ;
    byte[] byteArray, byteArrayIDC ;
    public String ConvertImage, ConvertImageIDC ;
    public String GetImageNameFromEditText;
    private int GALLERY = 1, CAMERA = 2;
    public int BUTTON_KEY = 0;
    //---------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form3);
        setToolBar();
        session = new SessionManager(getApplicationContext());
        //form image activity ------------------------
        GetImageFromGalleryButton = (Button)findViewById(R.id.buttonSelect);
        GetIDImageFromGalleryButton = (Button) findViewById(R.id.buttonSelectID);
        UploadImageOnServerButton = (Button)findViewById(R.id.buttonUpload);
        UploadIDImageOnServerButton = (Button)findViewById(R.id.buttonUploadID);
        //DoneButton = (Button) findViewById(R.id.btn_done);
        ShowSelectedImage = (ImageView)findViewById(R.id.imageView);
        ShowSelectedIDImage = (ImageView) findViewById(R.id.imageViewID);
        //---------------------------------------------

        mtegemeziJinaKamiliEditText = (EditText) findViewById(R.id.edit_mtegemeziJinaKamili);
        mtegemeziTareheKuzaliwaEditText = (EditText) findViewById(R.id.edit_mtegemeziTareheKuzaliwa);
        mtegemeziJinsiaEditText = (EditText) findViewById(R.id.mtegemeziJinsiaEditText);
        mtegemeziUhusianoEditText = (EditText) findViewById(R.id.mtegemeziUhusianoEditText);
        mtegemeziSimuNambaEditText = (EditText) findViewById(R.id.mtegemeziSimuNambaEditText);
        mtegemeziGenderSpinner = (Spinner) findViewById(R.id.spinner_gender_wategemezi);

        mtegemeziJinaKamiliEditText2 = (EditText) findViewById(R.id.edit_mtegemeziJinaKamili2);
        mtegemeziTareheKuzaliwaEditText2 = (EditText) findViewById(R.id.edit_mtegemeziTareheKuzaliwa2);
        mtegemeziUhusianoEditText2 = (EditText) findViewById(R.id.mtegemeziUhusianoEditText2);
        mtegemeziSimuNambaEditText2 = (EditText) findViewById(R.id.mtegemeziSimuNambaEditText2);
        mtegemeziGenderSpinner01 = (Spinner) findViewById(R.id.spinner_gender_wategemezi2);

        mtegemeziJinaKamiliEditText3 = (EditText) findViewById(R.id.edit_mtegemeziJinaKamili3);
        mtegemeziTareheKuzaliwaEditText3 = (EditText) findViewById(R.id.edit_mtegemeziTareheKuzaliwa3);
        mtegemeziUhusianoEditText3 = (EditText) findViewById(R.id.mtegemeziUhusianoEditText3);
        mtegemeziSimuNambaEditText3 = (EditText) findViewById(R.id.mtegemeziSimuNambaEditText3);
        mtegemeziGenderSpinner02 = (Spinner) findViewById(R.id.spinner_gender_wategemezi3);




        // viewsEditText = (EditText) findViewById(R.id.txtViewsAdd);
        //addButton = (Button) findViewById(R.id.btnAdd);

        Button buttonWekaPicha = (Button) findViewById(R.id.button_wekaPicha);

        //receive data from previous forms
        Intent intent = getIntent();
        mwanachamaJinaKamili = intent.getStringExtra(KEY_FULLNAME_MWANACHAMA);
        mwanachamaTareheKuzaliwa = intent.getStringExtra(KEY_DOB_MWANACHAMA);
        mwanachamaNdoa = intent.getStringExtra(KEY_NDOA_MWANACHAMA);
        mwanachamaJinsia = intent.getStringExtra(KEY_JINSIA_MWANACHAMA);
        mwanachamaAnuani = intent.getStringExtra(KEY_ANUANI_MWANACHAMA);
        mwanachamaWilaya = intent.getStringExtra(KEY_WILAYA_MWANACHAMA);
        mwanachamaMkoa = intent.getStringExtra(KEY_MKOA_MWANACHAMA);
        mwanachamaSimu = intent.getStringExtra(KEY_SIMU_MWANACHAMA);
        mwanachamaBaruaPepe = intent.getStringExtra(KEY_BARUA_MWANACHAMA);
        mwanachamaTareheUsajili = intent.getStringExtra(KEY_USAJILI_MWANACHAMA);
        member_id= utils.readPreferences(form3Activity.this,"member_id","");



//        ConvertImage = intent.getStringExtra(KEY_ImageTag);
//        GetImageNameFromEditText = intent.getStringExtra(KEY_ImageName);



        //Toast.makeText(getApplicationContext(),GetImageNameFromEditText,Toast.LENGTH_LONG).show();

        //Gender spinner
        ArrayAdapter<CharSequence> mtegemeziGenderAdpter = ArrayAdapter.createFromResource
                (this,R.array.array_gender,R.layout.support_simple_spinner_dropdown_item);
        mtegemeziGenderSpinner.setAdapter(mtegemeziGenderAdpter);
        mtegemeziGenderSpinner01.setAdapter(mtegemeziGenderAdpter);
        mtegemeziGenderSpinner02.setAdapter(mtegemeziGenderAdpter);
        mtegemeziGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mtegemeziJinsia = mtegemeziGenderSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* get the current date */
        startDate = Calendar.getInstance();
        /* get the current date */
        endDate = Calendar.getInstance();

        submitButton = (Button) findViewById(R.id.btn_submit);
        Button buttonBack = (Button) findViewById(R.id.btn_backInsubmit);
        mtegemeziTareheKuzaliwaEditText.setOnClickListener(this);
        mtegemeziTareheKuzaliwaEditText2.setOnClickListener(this);
        mtegemeziTareheKuzaliwaEditText3.setOnClickListener(this);
        submitButton.setOnClickListener(this);
        buttonBack.setOnClickListener(this);
        buttonWekaPicha.setOnClickListener(this);

        //--------------------------------------------------------
        imageName=(EditText)findViewById(R.id.imageName);
        byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStreamIDC = new ByteArrayOutputStream();

        GetImageFromGalleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPictureDialog();
                BUTTON_KEY = 3;


            }
        });

        GetIDImageFromGalleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
                BUTTON_KEY = 4;

            }
        });



        if (ContextCompat.checkSelfPermission(form3Activity.this, android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                        5);
            }
        }

        //-----------------------------------------------------------

    }


    private void setToolBar(){
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

@Override
public void onClick(View v) {
        switch (v.getId()){

            case R.id.edit_mtegemeziTareheKuzaliwa:

                showDateDialog(mtegemeziTareheKuzaliwaEditText, startDate);
               break;
            case R.id.edit_mtegemeziTareheKuzaliwa2:

                showDateDialog(mtegemeziTareheKuzaliwaEditText2, startDate);
                break;
            case R.id.edit_mtegemeziTareheKuzaliwa3:

                showDateDialog(mtegemeziTareheKuzaliwaEditText3, startDate);
                break;

            case R.id.btn_submit:
                if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {


                    ongezaMwanachama();
                    //Log.d("MEMBER-ID",member_id);


                    Toast.makeText(getApplicationContext(),"Data submitted",Toast.LENGTH_LONG).show();
//                Intent intent = new Intent(form3Activity.this,selectActivity.class);
//                startActivity(intent);
                } else {
                    Toast.makeText(form3Activity.this,
                            "Unable to connect to internet",
                            Toast.LENGTH_LONG).show();
                    return;

                }

                break;
            case R.id.btn_backInsubmit:
                this.finish();

                break;
            case R.id.button_wekaPicha:
                Intent picIntent = new Intent(form3Activity.this,imageUploadActivity.class);
                startActivity(picIntent);
                break;


        }

        }
           //TODO make ongezaMwanachama to process form field and picture submission, STATUS:DONE

    private void ongezaMwanachama() {

        //--------------------------------------------
        FixBitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
        FixBitmapIDC.compress(Bitmap.CompressFormat.JPEG,40, byteArrayOutputStreamIDC);

        byteArray = byteArrayOutputStream.toByteArray();
        byteArrayIDC = byteArrayOutputStreamIDC.toByteArray();


        ConvertImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
        ConvertImageIDC = Base64.encodeToString(byteArrayIDC, Base64.DEFAULT);

//        Toast.makeText(form3Activity.this,
//                ConvertImage,
//                Toast.LENGTH_LONG).show();
        //--------------------------------------------

        if (!STRING_EMPTY.equals(mtegemeziJinaKamiliEditText.getText().toString()) &&
                STRING_EMPTY.equals(mtegemeziJinsiaEditText.getText().toString()) &&
                !STRING_EMPTY.equals(mtegemeziUhusianoEditText.getText().toString())
                &&!STRING_EMPTY.equals(mtegemeziSimuNambaEditText.getText().toString())){

            mtegemeziJinaKamili = mtegemeziJinaKamiliEditText.getText().toString();
            mtegemeziTareheKuzaliwa = mtegemeziTareheKuzaliwaEditText.getText().toString();
            mtegemeziJinsia = mtegemeziGenderSpinner.getSelectedItem().toString();
            mtegemeziUhusiano = mtegemeziUhusianoEditText.getText().toString();
            mtegemeziSimuNamba = mtegemeziSimuNambaEditText.getText().toString();

            mtegemeziJinaKamili2 = mtegemeziJinaKamiliEditText2.getText().toString();
            mtegemeziTareheKuzaliwa2 = mtegemeziTareheKuzaliwaEditText2.getText().toString();
            mtegemeziJinsia2 = mtegemeziGenderSpinner01.getSelectedItem().toString();
            mtegemeziUhusiano2 = mtegemeziUhusianoEditText2.getText().toString();
            mtegemeziSimuNamba2 = mtegemeziSimuNambaEditText2.getText().toString();

            mtegemeziJinaKamili3 = mtegemeziJinaKamiliEditText3.getText().toString();
            mtegemeziTareheKuzaliwa3 = mtegemeziTareheKuzaliwaEditText3.getText().toString();
            mtegemeziJinsia3 = mtegemeziGenderSpinner02.getSelectedItem().toString();
            mtegemeziUhusiano3 = mtegemeziUhusianoEditText3.getText().toString();
            mtegemeziSimuNamba3 = mtegemeziSimuNambaEditText3.getText().toString();



            //mtegemeziJinsia = mtegemeziJinsiaEditText.getText().toString();

           // Log.d("TAG", mtegemeziTareheKuzaliwa+'-'+mtegemeziTareheKuzaliwa2+'-'+mtegemeziTareheKuzaliwa3);


            new ongezaMwanachamaAsyncTask().execute();
        } else {
            Toast.makeText(form3Activity.this,
                    "One or more fields left empty!",
                    Toast.LENGTH_LONG).show();

        }
    }

    private class ongezaMwanachamaAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display proggress bar
            pDialog = new ProgressDialog(form3Activity.this);
            pDialog.setMessage("Mwanachama anaongezwa. subiri...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpJsonParser httpJsonParser = new HttpJsonParser();
            Map<String, String> httpParams = new HashMap<>();
            //Populating request parameters


            //TODO merge picture parameters send them along with form field, STATUS:DONE
            //-----------------------------------------------
            //ImageProcessClass imageProcessClass = new ImageProcessClass();
            //HashMap<String,String> HashMapParams = new HashMap<String,String>();
            //HashMapParams.put(KEY_ImageTag, GetImageNameFromEditText);
            //HashMapParams.put(KEY_ImageName, ConvertImage);
            //-----------------------------------------------

            httpParams.put(KEY_FULLNAME_MTEGEMEZI, mtegemeziJinaKamili);
            httpParams.put(KEY_DOB_MTEGEMEZI, mtegemeziTareheKuzaliwa);
            httpParams.put(KEY_JINSIA_MTEGEMEZI, mtegemeziJinsia);
            httpParams.put(KEY_UHUSIANO_MTEGEMEZI, mtegemeziUhusiano);
            httpParams.put(KEY_SIMU_MTEGEMEZI,mtegemeziSimuNamba);
            httpParams.put(KEY_FULLNAME_MWANACHAMA,mwanachamaJinaKamili);
            httpParams.put(KEY_DOB_MWANACHAMA,mwanachamaTareheKuzaliwa);
            httpParams.put(KEY_NDOA_MWANACHAMA,mwanachamaNdoa);
            httpParams.put(KEY_JINSIA_MWANACHAMA,mwanachamaJinsia);
            httpParams.put(KEY_ANUANI_MWANACHAMA,mwanachamaAnuani);
            httpParams.put(KEY_WILAYA_MWANACHAMA,mwanachamaWilaya);
            httpParams.put(KEY_MKOA_MWANACHAMA,mwanachamaMkoa);
            httpParams.put(KEY_SIMU_MWANACHAMA,mwanachamaSimu);
            httpParams.put(KEY_BARUA_MWANACHAMA,mwanachamaBaruaPepe);
            httpParams.put(KEY_USAJILI_MWANACHAMA,mwanachamaTareheUsajili);

            httpParams.put(KEY_FULLNAME_MTEGEMEZI_WAPILI, mtegemeziJinaKamili2);
            httpParams.put(KEY_DOB_MTEGEMEZI_WAPILI, mtegemeziTareheKuzaliwa2);
            httpParams.put(KEY_JINSIA_MTEGEMEZI_WAPILI, mtegemeziJinsia2);
            httpParams.put(KEY_UHUSIANO_MTEGEMEZI_WAPILI, mtegemeziUhusiano2);
            httpParams.put(KEY_SIMU_MTEGEMEZI_WAPILI,mtegemeziSimuNamba2);

            httpParams.put(KEY_FULLNAME_MTEGEMEZI_WATATU, mtegemeziJinaKamili3);
            httpParams.put(KEY_DOB_MTEGEMEZI_WATATU, mtegemeziTareheKuzaliwa3);
            httpParams.put(KEY_JINSIA_MTEGEMEZI_WATATU, mtegemeziJinsia3);
            httpParams.put(KEY_UHUSIANO_MTEGEMEZI_WATATU, mtegemeziUhusiano3);
            httpParams.put(KEY_SIMU_MTEGEMEZI_WATATU,mtegemeziSimuNamba3);

            httpParams.put("status","1");
            httpParams.put("member_id",member_id);

            httpParams.put(KEY_ImageTag, GetImageNameFromEditText); // TODO: remove
            httpParams.put(KEY_ImageName, ConvertImage); // pass picture image string
            httpParams.put(KEY_ImageStringIDC, ConvertImageIDC); // pass identification card string



            //httpParams.put(KEY_VIEWS, views);
            JSONObject jsonObject = httpJsonParser.makeHttpRequest(
                    BASE_URL + "ongeza_mwanachama.php", "POST", httpParams);
            try {
                success = jsonObject.getInt(KEY_SUCCESS);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

    protected void onPostExecute(String result) {
        pDialog.dismiss();
        runOnUiThread(new Runnable() {
            public void run() {
                if (success == 1) {
                    //Display success message
                    Toast.makeText(form3Activity.this,
                            "Mwanachama Ameongezwa", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), selectActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                }else if (success == 0){

                    Toast.makeText(form3Activity.this,
                            "Error while adding (binding)", Toast.LENGTH_LONG).show();
                } else if (success == 2){
                    Toast.makeText(form3Activity.this,
                            "Mandatory parameter missing", Toast.LENGTH_LONG).show();
                } else if (success == 3){
                    Toast.makeText(form3Activity.this,
                            "Error in inserting", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}


    private void updateDisplay(EditText dateDisplay, Calendar date) {
        dateDisplay.setText(
                new StringBuilder()
                        // Month is 0 based so add 1
                        .append(date.get(Calendar.MONTH) + 1).append("-")
                        .append(date.get(Calendar.DAY_OF_MONTH)).append("-")
                        .append(date.get(Calendar.YEAR)).append(" "));

    }

    public void showDateDialog(EditText dateDisplay, Calendar date) {
        activeDateDisplay = dateDisplay;
        activeDate = date;
        showDialog(DATE_mtegemezi_ID);
    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            activeDate.set(Calendar.YEAR, year);
            activeDate.set(Calendar.MONTH, monthOfYear);
            activeDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDisplay(activeDateDisplay, activeDate);
            unregisterDateDisplay();
        }
    };

    private void unregisterDateDisplay() {
        activeDateDisplay = null;
        activeDate = null;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_mtegemezi_ID:
                return new DatePickerDialog(this, dateSetListener, activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
            case DATE_mtegemezi2_ID:
                return new DatePickerDialog(this, dateSetListener, activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
            case DATE_mtegemezi3_ID:
                return new DatePickerDialog(this, dateSetListener, activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));

        }
        return null;
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        switch (id) {
            case DATE_mtegemezi_ID:
                ((DatePickerDialog) dialog).updateDate(activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
                break;
            case DATE_mtegemezi2_ID:
                ((DatePickerDialog) dialog).updateDate(activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
                break;
            case DATE_mtegemezi3_ID:
                ((DatePickerDialog) dialog).updateDate(activeDate.get(Calendar.YEAR), activeDate.get(Calendar.MONTH), activeDate.get(Calendar.DAY_OF_MONTH));
                break;
        }
    }

    //------------------------------

    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Photo Gallery",
                "Camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    FixBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    FixBitmapIDC = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    // String path = saveImage(bitmap);
                    //Toast.makeText(MainActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    if(BUTTON_KEY == 3){
                        ShowSelectedImage.setImageBitmap(FixBitmap);
                        //UploadImageOnServerButton.setVisibility(View.VISIBLE);
                    } else if (BUTTON_KEY == 4){
                        ShowSelectedIDImage.setImageBitmap(FixBitmapIDC);
                        //UploadIDImageOnServerButton.setVisibility(View.VISIBLE);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(form3Activity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            FixBitmap = (Bitmap) data.getExtras().get("data");
            FixBitmapIDC = (Bitmap) data.getExtras().get("data");
            //ShowSelectedImage.setImageBitmap(FixBitmap);
            //UploadImageOnServerButton.setVisibility(View.VISIBLE);
            if(BUTTON_KEY == 3){
                ShowSelectedImage.setImageBitmap(FixBitmap);
                //UploadImageOnServerButton.setVisibility(View.VISIBLE);
            } else if (BUTTON_KEY == 4){
                ShowSelectedIDImage.setImageBitmap(FixBitmapIDC);
                //UploadIDImageOnServerButton.setVisibility(View.VISIBLE);
            }
            //  saveImage(thumbnail);
            //Toast.makeText(ShadiRegistrationPart5.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera

            }
            else {

                Toast.makeText(form3Activity.this, "Unable to use Camera..Please Allow us to use Camera", Toast.LENGTH_LONG).show();

            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.logout:
                session.setLogin(false);
                Intent intent = new Intent(getBaseContext(), loginActivity.class);
                startActivity(intent);
                finish();
                return true;
            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
