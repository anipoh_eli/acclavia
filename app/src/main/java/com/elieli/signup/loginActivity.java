package com.elieli.signup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.elieli.signup.Helper.CheckNetworkStatus;
import com.elieli.signup.Helper.HttpJsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class loginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String BASE_URL = "http://smartdarasa.com/movies/";
    private static String STRING_EMPTY = "";
    private static final String KEY_SUCCESS = "success";
    private static final String KEY_MEMBER = "member_id";
    private EditText usernameEditText,passwdEditText;
    private Button submitButton;
    private String username;
    private String passwd;
    private int success;
    private  int member_id;
    private SessionManager session;
    private ProgressDialog pDialog;
    private Context context=this;
    private Toolbar toolbar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        session = new SessionManager(getApplicationContext());

        if (session.isLoggedIn()) {
            Intent intent = new Intent(loginActivity.this, selectActivity.class);
            startActivity(intent);
            finish();
        }

        usernameEditText = (EditText) findViewById(R.id.username);
        passwdEditText = (EditText) findViewById(R.id.passwd);
        submitButton = (Button) findViewById(R.id.submit);
        submitButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){


            case R.id.submit:

                if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {

                    LoginMwanachama();

                } else {
                    Toast.makeText(loginActivity.this,
                            "Unable to connect to internet",
                            Toast.LENGTH_LONG).show();
                    return;

                }


        }

    }

    //TODO make ongezaMwanachama to process form field and picture submission, STATUS:DONE
    private void LoginMwanachama() {


        if (!STRING_EMPTY.equals(passwdEditText.getText().toString()) &&
                !STRING_EMPTY.equals(usernameEditText.getText().toString()))
               {

            username = passwdEditText.getText().toString();
            passwd = passwdEditText.getText().toString();


           // Log.d("TAG", beneficiaryDob+'-'+beneficiaryDob2+'-'+beneficiaryDob3);


            //views = viewsEditText.getText().toString();
            new LoginMwanachamaAsyncTask().execute();
        } else {
            Toast.makeText(loginActivity.this,
                    "One or more fields left empty!",
                    Toast.LENGTH_LONG).show();

        }
    }

    private class LoginMwanachamaAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display proggress bar
            pDialog = new ProgressDialog(loginActivity.this);
            pDialog.setMessage("Mwanachama anaongezwa. subiri...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpJsonParser httpJsonParser = new HttpJsonParser();
            Map<String, String> httpParams = new HashMap<>();
            //Populating request parameters

            httpParams.put("password", passwd);
            httpParams.put("username", username);
            httpParams.put("status", "0");

            JSONObject jsonObject = httpJsonParser.makeHttpRequest(
                    BASE_URL + "ongeza_mwanachama_nia.php", "POST", httpParams);
            try {
                success = jsonObject.getInt(KEY_SUCCESS);
                member_id = jsonObject.getInt(KEY_MEMBER);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    if (success == 1) {
                        session = new SessionManager(context);
                        utils.savePreferences(loginActivity.this, "member_id", String.valueOf(member_id));

                        session.setLogin(true);
                        // Launch loginActivity activity
                        Intent intent = new Intent(loginActivity.this, selectActivity.class);
                        startActivity(intent);
                        finish();

                    } else if (success == 0) {

                        Toast.makeText(loginActivity.this,
                                "Error while adding (binding)", Toast.LENGTH_LONG).show();
                    } else if (success == 2) {

                        session = new SessionManager(context);
                        utils.savePreferences(loginActivity.this, "member_id", "5");

                        session.setLogin(true);
                        // Launch loginActivity activity
                        Intent intent = new Intent(loginActivity.this, selectActivity.class);
                        startActivity(intent);
                        finish();



                        Toast.makeText(loginActivity.this,
                                "Mandatory parameter missing", Toast.LENGTH_LONG).show();
                    } else if (success == 3) {
                        Toast.makeText(loginActivity.this,
                                "Error in inserting", Toast.LENGTH_LONG).show();
                    }

                }
            });
        }

    }


}
